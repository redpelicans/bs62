"use strict";

var BASE62 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
i;

var base = function base(ALPHABET) {
  if (ALPHABET.length >= 255) throw new TypeError('Alphabet too long');
  var BASE_MAP = new Uint8Array(256);

  for (var j = 0; j < BASE_MAP.length; j++) {
    BASE_MAP[j] = 255;
  }

  for (var _i = 0; _i < ALPHABET.length; _i++) {
    var x = ALPHABET.charAt(_i);
    var xc = x.charCodeAt(0);
    if (BASE_MAP[xc] !== 255) throw new TypeError("".concat(x, " is ambiguous"));
    BASE_MAP[xc] = _i;
  }

  var BASE = ALPHABET.length;
  var LEADER = ALPHABET.charAt(0);
  var FACTOR = Math.log(BASE) / Math.log(256); // log(BASE) / log(256), rounded up

  var iFACTOR = Math.log(256) / Math.log(BASE); // log(256) / log(BASE), rounded up

  var encoder = new TextEncoder();
  var decoder = new TextDecoder();

  var encode = function encode(input) {
    var source = encoder.encode(input);
    if (source.length === 0) return ''; // Skip & count leading zeroes.

    var zeroes = 0;
    var length = 0;
    var pbegin = 0;
    var pend = source.length;

    while (pbegin !== pend && source[pbegin] === 0) {
      pbegin++;
      zeroes++;
    } // Allocate enough space in big-endian base58 representation.


    var size = (pend - pbegin) * iFACTOR + 1 >>> 0;
    var b58 = new Uint8Array(size); // Process the bytes.

    while (pbegin !== pend) {
      var carry = source[pbegin]; // Apply "b58 = b58 * 256 + ch".

      var _i2 = 0;

      for (var it1 = size - 1; (carry !== 0 || _i2 < length) && it1 !== -1; it1--, _i2++) {
        carry += 256 * b58[it1] >>> 0;
        b58[it1] = carry % BASE >>> 0;
        carry = carry / BASE >>> 0;
      }

      if (carry !== 0) throw new Error('Non-zero carry');
      length = _i2;
      pbegin++;
    } // Skip leading zeroes in base58 result.


    var it2 = size - length;

    while (it2 !== size && b58[it2] === 0) {
      it2++;
    } // Translate the result into a string.


    var str = LEADER.repeat(zeroes);

    for (; it2 < size; ++it2) {
      str += ALPHABET.charAt(b58[it2]);
    }

    return str;
  };

  var decodeUnsafe = function decodeUnsafe(source) {
    if (typeof source !== 'string') throw new TypeError('Expected String');
    if (source.length === 0) return new Uint8Array();
    var psz = 0; // Skip leading spaces.

    if (source[psz] === ' ') return; // Skip and count leading '1's.

    var zeroes = 0;
    var length = 0;

    while (source[psz] === LEADER) {
      zeroes++;
      psz++;
    } // Allocate enough space in big-endian base256 representation.


    var size = (source.length - psz) * FACTOR + 1 >>> 0; // log(58) / log(256), rounded up.

    var b256 = new Uint8Array(size); // Process the characters.

    while (source[psz]) {
      // Decode character
      var carry = BASE_MAP[source.charCodeAt(psz)]; // Invalid character

      if (carry === 255) return;
      var _i3 = 0;

      for (var it3 = size - 1; (carry !== 0 || _i3 < length) && it3 !== -1; it3--, _i3++) {
        carry += BASE * b256[it3] >>> 0;
        b256[it3] = carry % 256 >>> 0;
        carry = carry / 256 >>> 0;
      }

      if (carry !== 0) throw new Error('Non-zero carry');
      length = _i3;
      psz++;
    } // Skip trailing spaces.


    if (source[psz] === ' ') return; // Skip leading zeroes in b256.

    var it4 = size - length;

    while (it4 !== size && b256[it4] === 0) {
      it4++;
    }

    var vch = new Uint8Array(zeroes + (size - it4));
    vch.fill(0x00, 0, zeroes);
    var j = zeroes;

    while (it4 !== size) {
      vch[j++] = b256[it4++];
    }

    return vch;
  };

  var decode = function decode(string) {
    var buffer = decodeUnsafe(string);
    if (buffer) return decoder.decode(buffer);
    throw new Error("Non-base ".concat(BASE, " character"));
  };

  return {
    encode: encode,
    decode: decode
  };
};

module.exports = base(BASE62);

const base62 = require('..');
const baseX = require('base-x');
const fixtures = [
  'coucou',
  '',
  'ABS 123(Top)ics ﻮﺣﺩﺓ',
  'éé&#oô/+_ ,ç'
]

const BASE62 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const bx62 = baseX(BASE62);

const runTest = input => {
  it(`should encode "${input}"`, () => {
    expect(base62.encode(input)).toEqual(bx62.encode(Buffer.from(input)).toString());
  });

  const res = bx62.encode(Buffer.from(input)).toString();

  it(`should decode "${res}"`, () => {
    expect(base62.decode(res)).toEqual(input);
  });
};

describe('base62', () => {
  it('should encode and decode', () => {
    const input = 'coucou';
    expect(base62.decode(base62.encode(input))).toEqual(input);
  });

  describe('should get same results as base-x', () => {
    for(let input of fixtures) runTest(input);
  });
});

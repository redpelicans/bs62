# bs62

[![NPM Package](https://img.shields.io/npm/v/bs62.svg?style=flat-square)](https://www.npmjs.org/package/bs62)
[![Build Status](https://gitlab.com/redpelicans/bs62/badges/master/pipeline.svg)](https://gitlab.com/redpelicans/bs62/pipelines)


Fast base62 strings encoding / decoding using bitcoin style leading zero compression.


`bs62` is a rewrite of [cryptocoinjs/base-x](https://github.com/cryptocoinjs/base-x) without `Buffer` and limited to base62 and strings as input.

Target was to use the library in browsers.

@redpelicans we are using this lib to encode/decode keys using extended chars.

It's limited to below alphabet:

```
  const ALPHABET = `0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`;
```

## Example


``` javascript
var bs62 = require('bs62');

var decoded = bs62.encode('Bonjour ici les bidibules')
// 2XuGb0zE9QdU3OVBUGW1EAUlh2m8TJUioH

console.log(bs72.decode('2XuGb0zE9QdU3OVBUGW1EAUlh2m8TJUioH'))
// Bonjour ici ...
```


